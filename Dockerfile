# Sử dụng ảnh golang chính thức có tag là 1.16
FROM golang:1.21.1

# Thiết lập thư mục làm việc trong container
WORKDIR /app

# Sao chép mã nguồn và tệp go.mod vào container
COPY . .

# Biên dịch ứng dụng Golang
RUN go build -o my-golang-app .

# Khởi động ứng dụng khi container được chạy
CMD ["./my-golang-app"]
